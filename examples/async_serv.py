import asyncio
import socket


ADDR = ('127.0.0.1', 5556)


async def handle_connection(loop, cli):
    while True:
        data = await loop.sock_recv(cli, 1024)  # non blocking
        if not data:
            break
        await loop.sock_sendall(cli, data)  # non blocking


async def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(ADDR)
    sock.listen()
    sock.setblocking(False)

    loop = asyncio.get_event_loop()

    while True:
        cli, addr = await loop.sock_accept(sock)  # non blocking!
        print('Incoming connection from ', addr)
        loop.create_task(handle_connection(loop, cli))

# loop = asyncio.create_event_loop()
# loop.run_until_complete(run_server())

asyncio.run(main())
