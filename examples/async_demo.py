import asyncio
from threading import Thread


def task(n):
    for x in range(10):
        for y in range(10):
            print(f'Thread {n} {x} {y}')


t1 = Thread(target=task, args=(1,))
t2 = Thread(target=task, args=(2,))
t1.start()
t2.start()
t1.join()
t2.join()


async def task(n):
    for x in range(10):
        await asyncio.sleep(0.0001)
        for y in range(10):
            print(f'Coro {n} {x} {y}')


async def main():
    await asyncio.gather(task(1), task(2))


asyncio.run(main())
