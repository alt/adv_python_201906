import json
import time
from functools import wraps

import requests


def bounded(lower, upper):
    def inner(fn):
        @wraps(fn)
        def wrapper(*args):
            for arg in args:
                if lower > arg or arg > upper:
                    raise ValueError(
                        f'Arguments must be in range [{lower},{upper}]')
        return wrapper
    return inner


# xsub = bounded(0, 100)(add)


@bounded(0, 100)
def add(a, b):
    """Add numbers"""
    return a + b


@bounded(0, 100)
def sub(a, b):
    return a - b


def to_json(fn):
    def wrapper(*args, **kwargs):
        result = fn(*args, kwargs)
        return json.dumps(result)
    return wrapper


@to_json
def get_user(name):
    return {'username': 'Alex', 'email': 'a.tihonruk@gmail.com'}


def constant_wait(sec):
    return lambda _: sec


def exponential_wait(attempt):
    return 2**attempt - 1


no_wait = constant_wait(0)


def retry(num_attempts=10, exceptions=(Exception,), wait_fn=no_wait):
    def outer(fn):
        @wraps(fn)
        def inner(*args, **kwargs):
            ex = None
            for n in range(num_attempts):
                wait_sec = wait_fn(n)
                time.sleep(wait_sec)  # blocking!
                # await asyncio.wait(wait_sec)
                try:
                    return fn(*args, **kwargs)
                except exceptions as e:
                    ex = e
            raise ex
        return inner
    return outer


# get_data = retry(wait_fn=exponential_wait)(get_data)
# get_data = retry()(get_data)


@retry(num_attempts=3, wait_fn=exponential_wait)
def get_data():
    url = 'https://httpbin.org/status/404'
    res = requests.get(url)
    print(f'GET {url}: {res.status_code}')
    if res.status_code == requests.codes.ok:
        pass
    else:
        res.raise_for_status()
