from django import forms
from django.shortcuts import render


# Create your views here.

class AuthorForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    gender = forms.CharField()


class AuthorModelForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ('name', 'email', 'gender')

    
def index(request):
    pass


def create_author(request):
    ctx = {}

    if request.POST:
        form = AuthorForm(request.POST)        
    else:
        ctx['form'] = AuthorForm()
    return render('author.html', ctx)
