from django.db import models

# Create your models here.

GENDER_CHOICES = (
    ('F', 'Female'),
    ('M', 'Male'),
)


class Author(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)


class Book(models.Model):
    title = models.CharField(max_length=100)
    isbn = models.CharField(max_length=20)
    authors = models.ManyToManyField(Author)
    sold_count = models.PositiveIntegerField()

    def sale(self):
        ## INCORRECT, race condition
        ## update books_book set sold_count = 6
        # self.sold_count += 1
        # self.save()

        # update books_book set sold_count = sold_count + 1
        self.sold_count = F('sold_count') + 1
        self.save()
