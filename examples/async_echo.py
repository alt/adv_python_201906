import asyncio


async def handle_connection(reader, writer):
    while True:
        data = await reader.read(1024)
        if not data:
            break
        writer.write(data)
        await writer.drain()
    writer.close()


async def main():
    server = await asyncio.start_server(handle_connection,
                                        host='127.0.0.1', port=5555)
    async with server:
        await server.serve_forever()


asyncio.run(main())
