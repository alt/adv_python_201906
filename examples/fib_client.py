import time
from socket import *
from threading import Thread


REC_COUNT = 0


def counter():
    while True:
        global REC_COUNT
        time.sleep(1)
        print(REC_COUNT)
        REC_COUNT = 0


Thread(target=counter).start()


with socket(AF_INET, SOCK_STREAM) as sock:
    sock.connect(('127.0.0.1', 5557))
    while True:
        sock.sendall(b'1')
        data = sock.recv(1024)
        REC_COUNT += 1
        # print(data.decode())
