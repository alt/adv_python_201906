from collections import deque
from pprint import pprint


routes = {'KBP': ['AMS'],
          'AMS': ['KBP', 'BCN'],
          'BCN': ['AMS']}


def find_shortest_route(graph, start, stop):
    """Адаптированная и исправленная версия с
    https://www.python.org/doc/essays/graphs/"""

    dist = {start: [start]}
    queue = deque([start])
    while queue:
        nxt = queue.popleft()
        for conn in graph[nxt]:
            if conn not in dist:
                dist[conn] = [*dist[nxt], conn]
                queue.append(conn)
    # pprint(dist)
    return dist[stop]


find_shortest_route(routes, 'KBP', 'BCN')
