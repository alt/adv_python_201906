import time
from socket import *


with socket(AF_INET, SOCK_STREAM) as sock:
    sock.connect(('127.0.0.1', 5556))
    time.sleep(3)
    sock.sendall(b'Hello')
    data = sock.recv(1024)
    print(data.decode())
