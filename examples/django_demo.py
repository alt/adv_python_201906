from django.http import HttpResponse, JsonResponse
from django.urls import path
from django.core.management import execute_from_command_line
from django.conf import settings
from django.shortcuts import render

from pprint import pprint


settings.configure(
    DEBUG=True,
    ROOT_URLCONF=__name__,
            TEMPLATES=[{
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': ['']
        }]
)


def index(req):
    pprint(req.META)
    return JsonResponse({'message': 'Hello, Django'})


class Test:
    # def __getitem__(self, key):
    #    return key

    # def __getattr__(self, name):
    #    return 'Getattr ' + name 

    def __getitem__(self, idx):
        return 'Get' + str(idx)


def hello(req, name, greeting):
    names = ['Alex', 'John', 'Marie']
    return render(req, 'index.html', {
        'greeting': greeting,
        'names': names,
    })


urlpatterns = [
    path('', index),   # example.com/
    path('hello/<name>/<greeting>', hello),
    # url('', )  # regular expressions
    # import re
    # re_path('[]', )
]


if __name__ == '__main__':
    execute_from_command_line()
