class Money:
    __slots__ = ('amount', 'currency')

    def __init__(self, amount, currency):
        self.amount = amount
        self.currency = currency

    def __add__(self, other):
        if self.currency != other.currency:
            raise ValueError(
                'Cannot add {} to {}'.format(self.currency, other.currency))
        return Money(self.amount + other.amount, self.currency)

    def __str__(self):
        return '{} {}'.format(self.amount, self.currency)

    def __repr__(self):
        return 'Money({}, "{}")'.format(self.amount, self.currency)

    def __eq__(self, other):
        return type(self) == type(other)\
            and self.amount == other.amount\
            and self.currency == other.currency

    def __hash__(self):
        return hash((self.amount, self.currency))

    @staticmethod
    def convert(m1, m2):
        pass

    # !!! При наследовании все равно будет возвращать
    # родительский класс !!!
    # @staticmethod
    # def from_string(s):  # '10 UAH'
    #     a, c = s.split()
    #     return Money(a, c)

    @classmethod
    def from_string(cls, s):
        a, c = s.split()
        return cls(int(a), c)

    # Простой способ реализовать read-only атрибуты -
    # только геттеры.

    # @property
    # def amount(self):
    #     return self.__amount

    # @property
    # def currency(self):
    #     return self.__currency


class Account:
    def __init__(self, amount, currency):
        self.amount = amount
        self.currency = currency


class T:
    a = 1  # public (default)
    _b = 2  # "protected" (not really)
    __c = 3  # "private" (ніт)

    def __init__(self, d):
        self.d = d

    def set_a(self):
        self.a = 2

    def __call__(self):
        return self.a + self._b + self.__c

    def __getattr__(self, name):
        print('Getattr', name)

    def __getattribute__(self, name):
        print('Getattribute', name)
        return object.__gettattribute__(self, name)


# Множественное наследование

class X(T, Money):
    def __init__(self, d, e, f):
        super().__init__(d)
        self.e = e
        self.f = f


# Abstract classes. Калька с OOP языков, большого смысла в Python net
import abc


class AbstractMoney(abc.ABC):
    @abc.abstractmethod
    def add(self):
        raise NotImplementedError()


class AnotherMoney:
    def __init__(self, amount, currency):
        self.__amount = amount
        self.__currency = currency

    @property
    def amount(self):
        return self.__amount

    @amount.setter
    def amount(self, amount):
        self.__amount = amount


from decimal import Decimal
from dataclasses import dataclass

@dataclass
class Money:
    amount: Decimal
    currency: str
