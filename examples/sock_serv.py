from socket import *
from threading import Thread


ADDR = ('127.0.0.1', 5556)


class Handler(Thread):
    def run(self):
        pass


def handle_connection(cli, addr):
    with cli:
        while True:
            # cli.settimeout(1.0)
            data = cli.recv(1024)  # blocking!
            # cli.settimeout(None)
            if not data:
                break
            cli.sendall(data)  # blocking!


with socket(AF_INET, SOCK_STREAM) as sock:
    sock.bind(ADDR)
    sock.listen(1)
    print('Listening on ', ADDR)

    while True:
        cli, addr = sock.accept()  # blocking!
        print('Incoming connection from ', addr)
        t = Thread(target=handle_connection, args=(cli, addr))
        t.start()
