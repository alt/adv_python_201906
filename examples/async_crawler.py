import asyncio
from urllib.parse import urlsplit, urljoin

import aiohttp
import lxml.html


DOMAIN = 'https://python.org/'
NETLOC = urlsplit(DOMAIN).netloc

NUM_WORKERS = 10
MAX_PAGES = 100

enqueued = set()
count_visited = 0


async def worker(n, session, queue):
    global count_visited
    while True:
        if count_visited >= MAX_PAGES:
            break

        count_visited += 1
        url = await queue.get()
        print(f'[{n}] GET {url}')
        async with await session.get(url) as res:
            if res.status != 200:
                print(f'[{n}] Status {res.status} for {url}')
                continue
            try:
                txt = await res.text()
            except Exception as e:
                print(f'[{n}] Error: {url}', e)
                continue
            doc = lxml.html.fromstring(txt)  # memory consuming!
            # <a href="/">Main page</a>
            # <img src="">
            for elem, attr, link, _ in doc.iterlinks():
                link = urljoin(DOMAIN, link)
                if not urlsplit(link).netloc == NETLOC:
                    continue
                # /index.html => http://python.org/index.html
                if elem.tag == 'a' and attr == 'href':
                    if link not in enqueued:
                        enqueued.add(link)
                        queue.put_nowait(link)


async def start():
    queue = asyncio.Queue()
    queue.put_nowait(DOMAIN)
    async with aiohttp.ClientSession() as session:
        workers = [worker(n, session, queue) for n in range(NUM_WORKERS)]
        await asyncio.gather(*workers)


asyncio.run(start())
