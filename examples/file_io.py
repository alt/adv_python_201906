with open(__file__) as infile:
    # __enter__
    data = infile.read()
    # __exit__


class Connection:
    def __enter__(self):
        print('Enter')

    def __exit__(self, *args):
        print('Exit')
        print(args)



from contextlib import contextmanager, closing, redirect_stdout


@contextmanager
def connection():
    print('Enter')
    yield object()
    print('Exit')


with Connection() as conn:
    print('Do something with connection')


with closing(open(__file__)):
    pass


def dfib(n):
    a = b = 1
    while True:
        print(a)
        a, b = b, a + b


# redirect to file
with open('log.txt', 'w') as log:
    with redirect_stdout(log):
        print(dfib(5))
