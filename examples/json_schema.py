import json
import os
from pprint import pprint

import trafaret as T


SOURCES = {
    'ryanair':
    T.Dict({
        T.Key('airports'): T.List(
            T.Dict({
                T.Key('iataCode') >> 'code': T.String(),
                T.Key('routes'): T.List(
                    T.String() >> (lambda s: s.split(':', maxsplit=1))
                ) >> (lambda seq: [b for a, b in seq if a == 'airport'])
            }).ignore_extra('*'))
    }).ignore_extra('*'),
    'wizzair':
    T.List(
        T.Dict({
            T.Key('iata') >> 'code': T.String(),
            T.Key('connections') >> 'routes': T.List(
                T.Dict({
                    T.Key('iata') >> 'code': T.String()
                }).ignore_extra('*'))
        }).ignore_extra('*')
    ),
}


def load_data(path):
    for fqname in os.listdir(path):
        fname, _ = os.path.splitext(fqname)
        schema = SOURCES.get(fname)
        if schema:
            with open(os.path.join(path, fqname), 'rb') as datafile:
                data = json.load(datafile)
                pprint(schema.check(data))


if __name__ == '__main__':
    load_data('data')
