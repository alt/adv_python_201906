from socket import *
from threading import Thread
from concurrent.futures import ProcessPoolExecutor


process_pool = ProcessPoolExecutor(8)


ADDR = ('127.0.0.1', 5557)


def rfib(n):
    if n < 2:
        return 1
    else:
        return rfib(n - 1) + rfib(n - 2)


def handle_connection(cli, addr):
    with cli:
        while True:
            data = cli.recv(1024)  # blocking!
            if not data:
                break
            arg = int(data)
            fut = process_pool.submit(rfib, arg)
            res = str(fut.result()) + '\n'
            cli.sendall(res.encode())  # blocking!


with socket(AF_INET, SOCK_STREAM) as sock:
    sock.bind(ADDR)
    sock.listen(1)
    print('Listening on ', ADDR)

    while True:
        cli, addr = sock.accept()  # blocking!
        print('Incoming connection from ', addr)
        t = Thread(target=handle_connection, args=(cli, addr))
        t.start()
