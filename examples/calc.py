import tkinter as T

DOT, EVAL, CLEAR, MUL, ADD, SUB = '.=C*+-'

KEYBOARD = (
    (7, 8, 9, ADD),
    (4, 5, 6, SUB),
    (1, 2, 3, MUL),
    (0, DOT, EVAL, CLEAR),
)


def main():
    root = T.Tk()

    frame = T.Frame(root)
    frame.pack()

    text = T.StringVar()

    e = T.Entry(frame, textvariable=text)
    e.grid(row=0, columnspan=4)

    def on_press(key):
        def cmd():
            c = text.get()
            if key == CLEAR:
                text.set('')
            else:
                text.set(c + str(key))

        return cmd

    for irow, row in enumerate(KEYBOARD, 1):
        for ikey, key in enumerate(row):
            b = T.Button(frame, text=str(key), command=on_press(key))
            b.grid(column=ikey, row=irow)

    root.mainloop()


if __name__ == '__main__':
    main()
