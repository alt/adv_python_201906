from setuptools import setup

setup(
    name='pycalc',
    version='0.0.1',
    py_modules=['calc'],
    entry_points={
        'console_scripts': [
            'calc=calc:main'
        ]
    }
)
