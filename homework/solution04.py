# bencoding
# http://www.bittorrent.org/beps/bep_0003.html

"""
Strings are length-prefixed base ten followed by a colon and the string.
For example 4:spam corresponds to 'spam'.

>>> encode(b'spam')
b'4:spam'

Integers are represented by an 'i' followed by the number in base 10 followed by an 'e'.
For example i3e corresponds to 3 and i-3e corresponds to -3.
Integers have no size limitation. i-0e is invalid.
All encodings with a leading zero, such as i03e, are invalid,
other than i0e, which of course corresponds to 0.

>>> decode(b'i3e')
3
>>> decode(b'i-3e')
-3
>>> decode(b'i0e')
0
>>> decode(b'i03e')
Traceback (most recent call last):
  ...
ValueError: invalid literal for int() with base 0: '03'
 

Lists are encoded as an 'l' followed by their elements (also bencoded) followed by an 'e'.
For example l4:spam4:eggse corresponds to ['spam', 'eggs'].

>>> decode(b'l4:spam4:eggse')
[b'spam', b'eggs']

Dictionaries are encoded as a 'd' followed by a list of alternating keys
and their corresponding values followed by an 'e'.
For example, d3:cow3:moo4:spam4:eggse corresponds to {'cow': 'moo', 'spam': 'eggs'}
Keys must be strings and appear in sorted order (sorted as raw strings, not alphanumerics).

>>> decode(b'd3:cow3:moo4:spam4:eggse')
OrderedDict([(b'cow', b'moo'), (b'spam', b'eggs')])

"""
from functools import singledispatch
from collections import OrderedDict
from collections.abc import Sequence, Mapping


D, L, I, E, C = b'dlie:'


def encode_seq(val: typing.Iterable, out: bytearray, type_):
    out.append(type_)
    for e in val:
        encode_any(e, out)
    out.append(E)


def flatten(val: typing.Iterable[typing.Tuple[bytes, typing.Any]]):
    for k, v in val:
        yield k
        yield v

# AnyBencType = T.Union[T.AnyStr, T.List, T.Tuple, T.Mapping]

@singledispatch
def encode_any(val: T.Any, b):
    raise NotImplementedError


@encode_any.register(int)
def encode_int(val: int, out: bytearray) -> None:
    out.append(I)
    out.extend(str(val).encode())
    out.append(E)


@encode_any.register(bytes)
def encode_bytes(val: bytes, out: bytearray) -> None:
    out.extend(str(len(val)).encode())
    out.append(C)
    out.extend(val)


@encode_any.register(str)
def encode_str(val: str, out: bytearray) -> None:
    return encode_any(val.encode(), out)


@encode_any.register(Sequence)
def encode_list(val: T.Iterable, out) -> None:
    encode_seq(val, out, L)


@encode_any.register(OrderedDict)
def encode_ordered_dict(val: OrderedDict, out) -> None:
    encode_seq(flatten(val.items()), out, D)


@encode_any.register(Mapping)
def encode_dict(val: T.Mapping, out) -> None:
    encode_seq(flatten(sorted(val.items())), out, D)


def encode(val: T.Any) -> bytes:
    out = bytearray()
    encode_any(val, out)
    return bytes(out)


def decode_seq(bs: bytes, i: int) -> T.Tuple[T.List, int]:
    out: T.List[T.Any] = []
    while True:
        val, i = decode_any(bs, i)
        if val is None:
            return out, i
        else:
            out.append(val)


def decode_int(bs, i, stop=E) -> T.Tuple[int, int]:
    d = bs.index(stop, i)
    res = bs[i:d]
    return int(res), d + 1


def decode_bytes(bs, i) -> T.Tuple[bytes, int]:
    # 5:hello
    l, i = decode_int(bs, i, stop=C)
    res = bs[i:l]
    return res, i + l


def decode_dict(bs, i) -> T.Tuple[T.Dict[bytes, T.Any], int]:
    seq, i = decode_seq(bs, i)
    if len(seq) % 2 != 0:
        raise ValueError('')
    for k in seq[::2]:
        isinstance(k, bytes)
    cur = b''
    for k in seq[::2]:
        if k == cur:
            raise ValueError()
        if k < cur:
            raise ValueError()

    return OrderedDict(zip(*[iter(seq)]*2)), i


DECODE_DICT: T.Dict[int, T.Callable[[bytes, int], T.Tuple[T.Any, int]]] = {
    I: decode_int,
    L: decode_seq,
    D: decode_dict,
    E: lambda _, i: (None, i+1)
}


def decode_any(bs, i) -> T.Any:
    c = bs[i]
    while True:
        if c in DECODE_DICT:
            return DECODE_DICT[c](bs, i+1)
        else:
            return decode_bytes(bs, i)


def decode(bs: bytes) -> T.Any:
    res, _ = decode_any(bs, 0)
    return res


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.IGNORE_EXCEPTION_DETAIL)
